#!/usr/bin/python
"""
GIVEN A String
WHEN The first and last chars match
AND While iterating over the string the index adjusts by front +1 and back -1
THEN
IF all strings match print 'Is Palendrome'
ELSE print 'Not Palendrome'

=======
EXAMPLE
=======
./palendrome -s "civic" -v

c i v i c
+-------+ True
  +---+ True
Is Palendrome
"""
import argparse
import re

parser = argparse.ArgumentParser()
parser.add_argument('-s', '--string', required=True, type=str, help='STR: String to check for palindrome')
parser.add_argument('-v', '--verbose', help='Turn on verbose output', action='store_true')
args = parser.parse_args()

truth_table = []

def print_checks(word_array, result):
    if (args.verbose):
        print(''.join(word_array))
        space = len(word_array) - 2
        temp = "-" * space
        print("+%s+ %s") % (temp, str(result))

def check_palindrome(word):
    word_array = list(word)
    if (len(word_array) > 1):
        result = (word_array[0] == word_array [-1])
        truth_table.append(result)
        print_checks(word_array, result)
        del word_array[0]
        del word_array[-1]
        check_palindrome(word_array)

cleanString = re.sub('\W+','', args.string.lower() )
check_palindrome(cleanString)

outcome = (not False in truth_table)
print ("Palindrome %s") % (outcome)
