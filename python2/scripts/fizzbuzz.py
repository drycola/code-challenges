#!/bin/python


def fizzbuzz(min, max):
    output = ""
    if min > max:
        return
    if (min % 3 != 0 and min % 5 != 0):
        print(min, min)
        return
    if min % 3 == 0:
        output += "Fizz "
    if min % 5 == 0:
        output += "Buzz "
    print(min, output.rstrip())


for i in range(0, 50):
    fizzbuzz(i, 50)
