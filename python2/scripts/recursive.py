#!/usr/bin/python
"""
GIVEN An array of integers
AND A number of times to print
WHEN A recursive function is called and passed times to print
THEN Print array of integers x number of times
"""

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-t', '--times', required=True, type=int, help='INT: Times to print integers')
args = parser.parse_args()

int_array = [1,2,3,4,5]

def print_nums(times):
    if (times == 0):
        return
    else:
        times = times - 1
        print(int_array)
        print_nums(times)

print_nums(args.times)
