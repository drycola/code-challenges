#!/usr/bin/python
"""
GIVEN A max number to iterate to
AND A filter number to check for a mulitple
WHEN A mulitple is found
THEN Print multiple
"""
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-n', '--number', required=True, type=int, help='INT: Max number to check for filtered numbers')
parser.add_argument('-f', '--filter', required=True, type=int, help='INT: Display only numbers that are divisible by this number')
args = parser.parse_args()

div_numbers = []

start_number = 1
end_number = args.number

for x in range (start_number, end_number + 1):
    if (x%args.filter == 0):
        div_numbers.append(x)

print ("===== Mulitples of %s =====") % (args.filter)
print ("Count: %s") % (len(div_numbers))
print (div_numbers)
