from pydispatch import dispatcher


def main():
    SIGNAL = "ONE"
    recieve = Recieve(SIGNAL)
    dispatcher.send(signal="2", sender="main", args={"arg1": "test"})
    sender_one = SenderOne(SIGNAL)
    sender_one.send()


class Recieve():
    def __init__(self, SIGNAL):
        dispatcher.connect(self.handle_event, signal="1", sender=SIGNAL)
        dispatcher.connect(
            self.handle_event, signal="2", sender=dispatcher.Any)

    def handle_event(self, sender, args={}):
        """Simple event handler"""
        print('Signal was sent by', sender, args)


class SenderOne():
    def __init__(self, SIGNAL):
        self.SIGNAL = SIGNAL

    def send(self):
        dispatcher.send(signal="1", sender=self.SIGNAL)


if __name__ == "__main__":
    main()
